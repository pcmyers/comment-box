class CommentsController < ApplicationController
  include Validators
  before_filter :check_passphrase, :except => :passphrase

  def index
    @comments = Comment.all
  end

  def new
    @comment = Comment.new
  end

  def show
    @comment = Comment.find(params[:id])
  end

  def create
    @comment = Comment.new(params[:comment])

    respond_to do |format|
      if @comment.save!
        redirect_to comment_url(@comment)
      else
        render :action => "new"
      end
    end
  end

  def passphrase
    if params[:passphrase]
      @phrase = params[:passphrase]
      if Validators::Validator.check_phrase(@phrase)
        session[:passphrase] = @phrase
        redirect_to :controller => 'comments', :action => 'index'
      else
        flash[:notice] = "Passphrase not found. Try again."
      end
    end
  end

  def check_passphrase
    unless session[:passphrase]
      flash[:error] = "Please enter your passphrase"
      redirect_to root_url
    end
  end
end
