desc "Loads the dickens and creates one hundred passphrases"
task :populate => :environment do
  dickens = File.read("#{Rails.root}/lib/assets/pg98.txt")
  dickens = dickens.split
  dickens.each {|word| word.gsub!(/(\W|\d)/,"")}
  dickens.uniq!
  size = Key.all.size
  100.times do |phrase|
    phrase = dickens.sample(4).join(" ")
    Key.create(:phrase => phrase)
  end
  if Key.all.size == size + 100
    p "Succesfully added one hundred passphrases"
  else
    p "100 Keys were not added, for some reason. Added #{Key.all.size - size} keys"
  end
end
